/*
** my_fdf.c for  in /home/karmes_l/Projets/FDF/my_fdf/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec  2 17:32:26 2014 lionel karmes
** Last update Sun Dec  7 19:32:30 2014 lionel karmes
*/

#include "my.h"

void		free_ptr_coord(t_coord *ptr_coord)
{
  int		y;
  int		x;

  y = 0;
  while (y < ptr_coord->len_y)
    {
      x = 0;
      while (x < ptr_coord->len_x[y])
	{
	  free(ptr_coord->points[y][x]);
	  x++;
	}
      free(ptr_coord->points[y]);
      y++;
    }
  free(ptr_coord->points);
  free(ptr_coord->len_x);
}

void		my_fdf(char *path)
{
  t_coord	ptr_coord;

  sort_coord(path, &ptr_coord);
  draw_fdf(&ptr_coord);
}

void		draw_fdf(t_coord *ptr_coord)
{
  void		*mlx_ptr;
  void		*win_ptr;
  void		*img_ptr;

  if ((mlx_ptr = mlx_init()) == NULL)
    {
      my_putstr("value of mlx_init() : NULL\n");
      exit(1);
    }
  win_ptr = mlx_new_window(mlx_ptr, SIZE_WIN_X, SIZE_WIN_Y, "FDF");
  img_ptr = image(mlx_ptr, SIZE_WIN_X, SIZE_WIN_Y, ptr_coord);
  free_ptr_coord(ptr_coord);
  my_events(mlx_ptr, win_ptr, img_ptr);
}
