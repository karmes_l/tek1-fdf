/*
** main.c for  in /home/karmes_l/Projets/FDF/my_fdf/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec  2 17:31:46 2014 lionel karmes
** Last update Tue Dec  2 17:40:17 2014 lionel karmes
*/

#include "my.h"

int	main(int ac, char **av)
{
  if (ac > 1)
    my_fdf(av[1]);
  else
    my_putstr("Usage : ./fdf [FILE]");
  return (0);
}
