/*
** init_droite.c for  in /home/karmes_l/Projets/FDF/test/droites
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Nov 17 12:16:54 2014 lionel karmes
** Last update Sun Dec  7 19:33:19 2014 lionel karmes
*/

#include "my.h"

void	init_droite(t_droite *ptr_droite, t_coord *ptr_coord, int x, int y)
{
  int	scale_x;

  scale_x = max_value(ptr_coord->len_x, ptr_coord->len_y) - 1;
  ptr_droite->x1 = x * (SIZE_WIN_X / scale_x);
  ptr_droite->z1 = ptr_coord->points[y][x][0];
  ptr_droite->x2 = x * (SIZE_WIN_X / scale_x) + (SIZE_WIN_X / scale_x);
  if (ptr_coord->len_y <= 1)
    {
      ptr_droite->y1 = y * SIZE_WIN_Y;
      ptr_droite->y2 = y * SIZE_WIN_Y;
    }
  else
    {
      ptr_droite->y1 = y * (SIZE_WIN_Y / (ptr_coord->len_y - 1));
      ptr_droite->y2 = y * (SIZE_WIN_Y / (ptr_coord->len_y - 1));
    }
  ptr_droite->z2 = ptr_coord->points[y][x + 1][0];
  isometric_project_droite(ptr_droite);
}

void	init_droite2(t_droite *ptr_droite, t_coord *ptr_coord, int x, int y)
{
  int	scale_x;

  if (max_value(ptr_coord->len_x, ptr_coord->len_y) <= 1)
    scale_x = 1;
  else
    scale_x = max_value(ptr_coord->len_x, ptr_coord->len_y) - 1;
  ptr_droite->x1 = x * (SIZE_WIN_X / scale_x);
  ptr_droite->y1 = y * (SIZE_WIN_Y / (ptr_coord->len_y - 1));
  ptr_droite->z1 = ptr_coord->points[y][x][0];
  ptr_droite->x2 = x * (SIZE_WIN_X / scale_x);
  ptr_droite->y2 = y * (SIZE_WIN_Y / (ptr_coord->len_y - 1))
    + (SIZE_WIN_X / (ptr_coord->len_y - 1));
  ptr_droite->z2 = ptr_coord->points[y + 1][x][0];
  isometric_project_droite(ptr_droite);
}
