/*
** my_events.c for  in /home/karmes_l/TP/tp1_minilibx_et_evenements/ex3
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Nov 10 15:18:33 2014 lionel karmes
** Last update Sun Dec  7 19:33:42 2014 lionel karmes
*/

#include "my.h"

int		gere_key(int keycode, void *param)
{
  t_param	*mlx;

  mlx = param;
  mlx->mlx_ptr = mlx->mlx_ptr;
  if (keycode == 65307)
    exit(1);
  return (0);
}

int		gere_expose(void *param)
{
  t_param	*mlx;

  mlx = param;
  my_putstr("Actualisation de la fenêtre");
  mlx_put_image_to_window(mlx->mlx_ptr, mlx->win_ptr, mlx->img_ptr, 0, 0);
  my_putchar('\n');
  return (0);
}

void		my_events(void *mlx_ptr, void *win_ptr, void *img_ptr)
{
  t_param	param;

  param.mlx_ptr = mlx_ptr;
  param.win_ptr = win_ptr;
  param.img_ptr = img_ptr;
  mlx_key_hook(win_ptr, gere_key, &param);
  mlx_expose_hook(win_ptr, gere_expose, &param);
  mlx_loop(mlx_ptr);
}
