/*
** image.c for  in /home/karmes_l/Projets/FDF/test
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Nov  8 10:13:45 2014 lionel karmes
** Last update Sun Dec  7 19:30:20 2014 lionel karmes
*/

#include "my.h"

int		max_value(int *nbr, int size)
{
  int		i;
  int		nbr_max;

  i = 0;
  if (size <= 0)
    return (0);
  nbr_max = nbr[0];
  while (i < size)
    {
      if (nbr_max < nbr[i])
	nbr_max = nbr[i];
      i++;
    }
  return (nbr_max);
}

void		droite_y(void *mlx_ptr, t_data_img *ptr_data_img, t_coord *ptr_coord,
			 t_droite *ptr_droite)
{
  int		x;
  int		y;

  y = 0;
  while (y < ptr_coord->len_y)
    {
      x = 0;
      while (x < ptr_coord->len_x[y] - 1)
	{
	  init_droite(ptr_droite, ptr_coord, x, y);
	  droite_img(ptr_data_img, ptr_droite,
		     mlx_get_color_value(mlx_ptr, ptr_coord->points[y][x][1]));
	  x++;
	}
      y++;
    }
}

void		droite_x(void *mlx_ptr, t_data_img *ptr_data_img, t_coord *ptr_coord,
		 t_droite *ptr_droite)
{
  int		x;
  int		y;
  int		max_value_x;

  x = 0;
  max_value_x = max_value(ptr_coord->len_x, ptr_coord->len_y);
  while (x < max_value_x)
    {
      y = 0;
      while (y < ptr_coord->len_y - 1)
  	{
	  if (ptr_coord->len_x[y] > x && ptr_coord->len_x[y + 1] > x)
	    {
	      init_droite2(ptr_droite, ptr_coord, x, y);
	      droite_img(ptr_data_img, ptr_droite,
			 mlx_get_color_value(mlx_ptr, ptr_coord->points[y][x][1]));
	    }
  	  y++;
  	}
      x++;
    }
}

void		draw_image(void *mlx_ptr, t_data_img *ptr_data_img, t_coord *ptr_coord)
{
  t_droite	ptr_droite;

  droite_y(mlx_ptr, ptr_data_img, ptr_coord, &ptr_droite);
  droite_x(mlx_ptr, ptr_data_img, ptr_coord, &ptr_droite);
}

void		*image(void *mlx_ptr, int x, int y, t_coord *ptr_coord)
{
  void		*img_ptr;
  char		*data;
  int		bits_per_pixel;
  int		size_line;
  int		endian;
  t_data_img	ptr_data_img;

  img_ptr = mlx_new_image(mlx_ptr, x, y);
  data = mlx_get_data_addr(img_ptr, &bits_per_pixel, &size_line, &endian);
  ptr_data_img = init_data_img(img_ptr, data, bits_per_pixel, size_line);
  draw_image(mlx_ptr, &ptr_data_img, ptr_coord);
  return (img_ptr);
}
