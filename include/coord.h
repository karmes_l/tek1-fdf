/*
** coord.h for  in /home/karmes_l/Projets/FDF/my_fdf/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Dec  3 10:14:39 2014 lionel karmes
** Last update Sun Dec  7 11:46:02 2014 lionel karmes
*/

#ifndef _COORD_H_
# define _COORD_H_

typedef struct	s_coord
{
  int		***points;
  int		len_y;
  int		*len_x;
}		t_coord;

#endif
