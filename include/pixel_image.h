/*
** pixel_image.h for  in /home/karmes_l/Projets/FDF/test/lib/my
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Nov 17 15:19:15 2014 lionel karmes
** Last update Fri Dec  5 12:03:20 2014 lionel karmes
*/

#ifndef PIXEL_IMAGE_H_
# define PIXEL_IMAGE_H_

typedef struct	s_pixel
{
  int		x1;
  int		y1;
  int		color;
}		t_pixel;

typedef struct	s_data_img
{
  void		*img_ptr;
  char		*data;
  int		bits_per_pixel;
  int		size_line;
}		t_data_img;

typedef struct	s_param
{
  void		*mlx_ptr;
  void		*win_ptr;
  void		*img_ptr;
}		t_param;

#endif /* !PIXEL_IMAGE_H_ */
