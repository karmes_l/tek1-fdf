/*
** image.c for  in /home/karmes_l/Projets/FDF/test
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Nov  8 10:13:45 2014 lionel karmes
** Last update Sun Dec  7 19:30:59 2014 lionel karmes
*/

#include "my.h"

t_data_img	init_data_img(void *img_ptr, char *data, int bits_per_pixel,
			      int size_line)
{
  t_data_img	ptr_data_img;

  ptr_data_img.img_ptr = img_ptr;
  ptr_data_img.data = data;
  ptr_data_img.bits_per_pixel = bits_per_pixel;
  ptr_data_img.size_line = size_line;
  return (ptr_data_img);
}

void		mlx_pixel_put_image(t_data_img *ptr_data_img, int x, int y,
				    int color)
{
  unsigned long	pixel;

  if (x < SIZE_WIN_X && x >= 0 && y < SIZE_WIN_Y && y >= 0)
    {
      pixel = x * (ptr_data_img->bits_per_pixel / 8)
	+ ptr_data_img->size_line * y;
      ptr_data_img->data[pixel] = color;
      ptr_data_img->data[pixel + 1] = color >> 8;
      ptr_data_img->data[pixel + 2] = color >> 16;
    }
}
