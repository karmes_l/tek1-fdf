##
## Makefile for  in /home/karmes_l/Projets/Maths
## 
## Made by lionel karmes
## Login   <karmes_l@epitech.net>
## 
## Started on  Mon Nov  3 16:51:51 2014 lionel karmes
## Last update Sun Dec  7 12:14:31 2014 lionel karmes
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -Wextra -Wall -Werror
CFLAGS	+= -ansi -pedantic
CFLAGS	+= -I./include/

LDFLAGS	=

NAME	= fdf

LIB	= libmy

SRCS	= main.c \
	droite_img.c \
	fct_image.c \
	get_next_line.c \
	image.c \
	isometric_project_droite.c \
	init_droite.c \
	my_events.c \
	my_fdf.c \
	my_strnum_to_wordtab.c \
	sort_coord.c \
	my_tabstrlen.c \
	sort_coord2.c

OBJS	= $(SRCS:.c=.o)


all: $(NAME)

$(NAME): $(OBJS)
	make -C lib/
	make -C minilibx/
	$(CC) $(OBJS) -o $(NAME) $(LDFLAGS) -L./lib/ -lmy -L./minilibx/ -lmlx_$(HOSTTYPE) -L/usr/lib64/X11 -lXext -lX11 -I./include/

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
