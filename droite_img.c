/*
** droite.c for  in /home/karmes_l/Projets/FDF/test/droites
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Nov 17 12:01:32 2014 lionel karmes
** Last update Sun Dec  7 19:31:50 2014 lionel karmes
*/

#include "my.h"

int	droite1_img(t_data_img *ptr_data_img, t_droite *coord, int color)
{
  int	y;
  int	x;

  if (coord->x2 - coord->x1 == 0)
    {
      y = coord->y2;
      while (y <= coord->y1)
	mlx_pixel_put_image(ptr_data_img, coord->x1, y++, color);
    }
  if (coord->y2 - coord->y1 == 0)
    {
      x = coord->x2;
      while (x <= coord->x1)
	mlx_pixel_put_image(ptr_data_img, x++, coord->y1, color);
    }
  return (0);
}

int	droite2_img(t_data_img *ptr_data_img, t_droite *coord, int color)
{
  int	x;

  if (abs(coord->x2 - coord->x1) >= abs(coord->y2 - coord->y1))
    {
      if ((coord->x1 > coord->x2 && coord->y1 < coord->y2) ||
	  (coord->x1 > coord->x2 && coord->y1 > coord->y2))
	{
	  x = coord->x1;
	  coord->x1 = coord->x2;
	  coord->x2 = x;
	  x = coord->y1;
	  coord->y1 = coord->y2;
	  coord->y2 = x;
	}
      x = coord->x1;
      while (x <= coord->x2)
	{
	  mlx_pixel_put_image(ptr_data_img, x, coord->y1
			      + (coord->y2 - coord->y1) * (x - coord->x1) / (coord->x2 - coord->x1),
			      color);
	  x++;
	}
      return (1);
    }
  return (0);
}

int	droite3_img(t_data_img *ptr_data_img, t_droite *coord, int color)
{
  int	y;

  if (abs(coord->x2 - coord->x1) < abs(coord->y2 - coord->y1))
    {
      if ((coord->x1 < coord->x2 && coord->y1 > coord->y2) ||
	  (coord->x1 > coord->x2 && coord->y1 > coord->y2))
	{
	  y = coord->x1;
	  coord->x1 = coord->x2;
	  coord->x2 = y;
	  y = coord->y1;
	  coord->y1 = coord->y2;
	  coord->y2 = y;
	}
      y = coord->y1;
      while (y <= coord->y2)
	{
	  mlx_pixel_put_image(ptr_data_img, coord->x1
			      + ((coord->x2 - coord->x1) * (y - coord->y1)) / (coord->y2 - coord->y1),
			      y, color);
	  y++;
	}
      return (1);
    }
  return (0);
}

void	droite_img(t_data_img *ptr_data_img, t_droite *coord, int color)
{
  int	(*ptr_droites[3])(t_data_img *ptr_data_img, t_droite *coord, int color);
  int	i;

  i = 0;
  ptr_droites[0] = &droite1_img;
  ptr_droites[1] = &droite2_img;
  ptr_droites[2] = &droite3_img;
  while (i < 3 && !ptr_droites[i](ptr_data_img, coord, color))
    i++;
}
