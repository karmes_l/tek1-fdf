/*
** isometric_project_droite.c for  in /home/karmes_l/Projets/FDF/my_fdf/v2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun Dec  7 15:54:08 2014 lionel karmes
** Last update Sun Dec  7 19:32:49 2014 lionel karmes
*/

#include "my.h"

void	isometric_project_droite(t_droite *coord)
{
  float cte;
  float cte2;
  int	tmp_x1;
  int	tmp_x2;

  cte = 0.5;
  cte2 = 0.5;
  tmp_x1 = coord->x1;
  tmp_x2 = coord->x2;
  coord->x1 = SIZE_WIN_X / 2 + (cte * coord->x1 - cte2 * coord->y1);
  coord->y1 = SIZE_WIN_Y -
    (coord->z1 + (cte / 2) * tmp_x1 + (cte2 / 2) * coord->y1);
  coord->x2 = SIZE_WIN_X / 2 + (cte * coord->x2 - cte2 * coord->y2);
  coord->y2 = SIZE_WIN_Y -
    (coord->z2 + (cte / 2) * tmp_x2 + (cte2 / 2) * coord->y2);
}
