/*
** sort_coord2.c for  in /home/karmes_l/Projets/FDF/my_fdf/v2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Dec  6 23:05:00 2014 lionel karmes
** Last update Sun Dec  7 19:29:22 2014 lionel karmes
*/

#include "my.h"

char	*altitud_str(char *str)
{
  int	i;
  char	*profond_str;

  i = 0;
  if ((profond_str = malloc(sizeof(char) * (my_strlen(str) + 1))) == NULL)
    exit(0);
  while (str[i] != '\0')
    {
      if (str[i] == ',')
	{
	  profond_str[i] = '\0';
	  return (profond_str);
	}
      profond_str[i] = str[i];
      i++;
    }
  profond_str[i] = '\0';
  return (profond_str);
}

int	altitud(char *str)
{
  int	profond;
  char	*profond_str;

  profond_str = altitud_str(str);
  profond = my_getnbr(profond_str);
  free(profond_str);
  return (profond);
}

char	*color_hex_str(char *str)
{
  int	i;
  int	j;
  char	*color_str;

  i = 0;
  while (str[i] != '\0' && (str[i] != '0' || str[i + 1] != 'x'))
    i++;
  j = 0;
  while (str[i] != '\0' && j++ < 2)
    i++;
  if ((color_str = malloc(sizeof(char) * (my_strlen(str) + 1 - i))) == NULL)
    exit(0);
  j = 0;
  while (str[i] != '\0')
    {
      color_str[j] = str[i];
      j++;
      i++;
    }
  color_str[j] = '\0';
  return (color_str);
}

int	color_hex(char *str)
{
  int	color;
  char	*color_str;
  char	*color_str_b10;

  color_str = color_hex_str(str);
  if (my_strlen(color_str) == 0)
    color = 0xFFFFFF;
  else
    {
      color_str_b10 = convert_base(color_str, "0123456789ABCDEF",
      				   "0123456789");
      color = my_getnbr(color_str_b10);
      free(color_str_b10);
    }
  free(color_str);
  return (color);
}
