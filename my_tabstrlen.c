/*
** my_tabstrlen.c for  in /home/karmes_l/Projets/FDF/my_fdf/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Fri Dec  5 14:16:40 2014 lionel karmes
** Last update Fri Dec  5 14:18:22 2014 lionel karmes
*/

int	my_tabstrlen(char **word)
{
  int	j;

  j = 0;
  while (word[j] != 0)
    j++;
  return (j);
}
