/*
** sort_coord.c for  in /home/karmes_l/Projets/FDF/my_fdf/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec  2 17:35:52 2014 lionel karmes
** Last update Sun Dec  7 16:08:37 2014 lionel karmes
*/

#include "my.h"

int	nbr_line_file(char *path)
{
  int	fd;
  int	line;
  char	*s;

  fd = open(path, O_RDONLY);
  if (fd < 0)
    {
      my_putstr(strerror(errno));
      exit(errno);
    }
  line = 0;
  while ((s = get_next_line(fd)) != NULL)
    {
      line++;
      if (s[0] == '\0')
	line--;
      free(s);
    }
  close(fd);
  return (line);
}

void	free_strnum_to_wordtab(char **word, int nbr_word)
{
  int	i;

  i = 0;
  while (i < nbr_word)
    {
      free(word[i]);
      i++;
    }
  free(word);
}

void	sort_coord_next2(char **str_coord, int y, t_coord *ptr_coord)
{
  int	x;

  x = 0;
  while (str_coord[x] != 0)
    {
      if ((ptr_coord->points[y][x] =
	   malloc(sizeof(int *) * 2)) == NULL)
	exit(0);
      ptr_coord->points[y][x][0] = altitud(str_coord[x]);
      ptr_coord->points[y][x][1] = color_hex(str_coord[x]);
      free(str_coord[x]);
      x++;
    }
}

void	sort_coord_next(int fd, t_coord *ptr_coord)
{
  int	y;
  char	*s;
  char	**str_coord;

  y = ptr_coord->len_y - 1;
  while ((s = get_next_line(fd)) != NULL && y >= 0)
    {
      if (s[0] != '\0')
	{
	  str_coord = my_strnum_to_wordtab(s);
	  ptr_coord->len_x[y] = my_tabstrlen(str_coord);
	  if ((ptr_coord->points[y] = malloc(sizeof(int **) * ptr_coord->len_x[y]))
	      == NULL)
	    exit(0);
	  sort_coord_next2(str_coord, y, ptr_coord);
	  y--;
	  free(str_coord);
	  free(s);
	}
    }
}

void	sort_coord(char *path, t_coord *ptr_coord)
{
  int	fd;

  ptr_coord->len_y = nbr_line_file(path);
  if ((ptr_coord->len_x = malloc(sizeof(int *) * ptr_coord->len_y)) == NULL)
    exit(0);
  if ((ptr_coord->points = malloc(sizeof(int ***) * ptr_coord->len_y)) == NULL)
    exit(0);
  fd = open(path, O_RDONLY);
  if (fd < 0)
    {
      my_putstr(strerror(errno));
      exit(errno);
    }
  sort_coord_next(fd, ptr_coord);
  close(fd);
}
