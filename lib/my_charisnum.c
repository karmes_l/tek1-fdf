/*
** my_charisnum.c for  in /home/karmes_l/test/tmp_Piscine_C_J11/do-op
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Oct 20 15:28:15 2014 lionel karmes
** Last update Mon Oct 20 15:29:44 2014 lionel karmes
*/

int	my_charisnum(char c)
{
  if (c >= 48 && c <=57)
    return (0);
  return (1);
}
