/*
** droite.h for  in /home/karmes_l/Projets/FDF/test/include
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Nov 17 12:07:10 2014 lionel karmes
** Last update Tue Nov 18 16:22:39 2014 lionel karmes
*/

#ifndef FIGURE_H_
# define FIGURE_H_

# define PI	(3.14159265359)

typedef	struct	s_droite
{
  int		x1;
  int		y1;
  int		x2;
  int		y2;
  int		z1;
  int		z2;
}		t_droite;

typedef struct	s_ellipse
{
  int		rayon1;
  int		rayon2;
  int		x1;
  int		y1;
  int		z1;
}		t_ellipse;

#endif /* !FIGURE_H_ */
