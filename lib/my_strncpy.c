/*
** my_strncpy.c for  in /home/karmes_l/test/tmp_Piscine_C_J06
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Oct  6 16:11:48 2014 lionel karmes
** Last update Wed Oct  8 10:46:18 2014 lionel karmes
*/

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;

  i = 0;
  while (i < n && src[i] != '\0')
    {
      dest[i] = src[i];
      i = i + 1;
    }
  if (src[i] == '\0')
    {
      dest[i + 1] = '\0';
    }
  return dest;
}
