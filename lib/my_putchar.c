/*
** my_putchar.c for  in /home/karmes_l/test/my_function
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Oct  8 09:38:56 2014 lionel karmes
** Last update Fri Dec  5 12:17:36 2014 lionel karmes
*/

#include "my.h"

void	my_putchar(char c)
{
  write(1, &c, 1);
}
